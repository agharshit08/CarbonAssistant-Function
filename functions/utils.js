const {
  SimpleResponse, Suggestions
} = require('actions-on-google');

exports.roundWithPrecision = (value, precision) => {
  var multiplier = Math.pow(10, precision || 0);
  return Math.round(value * multiplier) / multiplier;
}

exports.getRandomNumber = (minimum, maximum) => {
  return Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
}
exports.richResponse = (conv, display, toSpeak) => {
  conv.ask(new SimpleResponse({
    speech: toSpeak,
    text: display
  }));
  let randomNumber = Math.floor(Math.random() * 5);

  // Poultry
  if (randomNumber == 0) {
    let randomPoultry = Math.floor(Math.random() * 5);
    let typeOfPoultry = '';
    if (randomPoultry == 0)
      typeOfPoultry = 'egg';
    else if (randomPoultry == 1)
      typeOfPoultry = 'broiler chicken';
    else if (randomPoultry == 2)
      typeOfPoultry = 'pork';
    else if (randomPoultry == 3)
      typeOfPoultry = 'beef';
    else if (randomPoultry == 4)
      typeOfPoultry = 'lamp';
    else if (randomPoultry == 5)
      typeOfPoultry = 'turkey';

    conv.ask(`<speak><break time="3"/> Another interesting information you can get about emissions caused by poultry. You can ask something like, "How much emissions are caused by ${randomPoultry + 1} kg of ${typeOfPoultry} production in New York?"</speak>`);
  }

  // Appliances
  else if (randomNumber == 1) {
    let randomAppliance = Math.floor(Math.random() * 4);
    let typeOfAppliance = '';
    let sizeOfAppliance = '';
    if (randomAppliance == 0) {
      typeOfAppliance = 'Air Conditioner';
      sizeOfAppliance = 'small';
    }
    else if (randomAppliance == 1) {
      typeOfAppliance = 'CFL';
      sizeOfAppliance = 'large';
    }
    else if (randomAppliance == 2) {
      typeOfAppliance = 'Television';
      sizeOfAppliance = 'small';
    }
    else if (randomAppliance == 3) {
      typeOfAppliance = 'Air Cooler';
      sizeOfAppliance = 'larger';
    }
    else if (randomAppliance == 4) {
      typeOfAppliance = 'Hair Curler';
      sizeOfAppliance = 'larger';
    }
    conv.ask(`<speak><break time="3"/> Electrical appliances made our life easier than ever but they also emit harmful radiations, Get this interesting data by asking something like "What are emisions for a small air conditioner in India?"</speak>`);
  }
  // Flights
  else if (randomNumber == 2) {
    conv.ask(`<speak><break time="3"/> Ever wondered how much emissions caused by a comfortable flight between two destinations, Try this out by asking something like "emissions for flight from New York to Washington with 620 passengers"</speak>`);
  }

  // Fuels
  else if (randomNumber == 3) {
    let randomFuel = Math.floor(Math.random() * 5);
    let typeOfFuel = '';
    if (randomFuel == 0)
      typeOfFuel = 'petrol';
    else if (randomFuel == 1)
      typeOfFuel = 'diesel';
    else if (randomFuel == 2)
      typeOfFuel = 'gasoline';
    else if (randomFuel == 3)
      typeOfFuel = 'ethanol';
    else if (randomFuel == 4)
      typeOfFuel = 'CNG';
    else if (randomFuel == 5)
      typeOfFuel = 'LPG';
    conv.ask(`<speak><break time="3"/> You can also ask me about the pollution caused by 10 litres of ${typeOfFuel} </speak>`);
  }

  // Trains
  else if (randomNumber == 4) {
    conv.ask(`<speak><break time="3"/> Trains are considered as one of the most convenient mode of transport and so is the pollution caused due to them, get this data by asking something like "Pollution caused due to travel from Chennai to Bangalore by train" </speak>`);
  }

  // Vehicle
  else if (randomNumber == 5) {
    conv.ask(`<speak><break time="3"/> Travelling is the part of everyday life and due to that our environment is affecting, Don't forget to check that out by asking something like "Emissions for travel from New York to Washington in petrol car" </speak>`);
  }
}

exports.responseWithSuggestions = (conv, display, suggestions) => {
  console.log("Sending response: " + display);
  console.log("Sending suggestion: " + suggestions);
  conv.ask(new SimpleResponse({
    speech: display,
    text: display
  }),
    new Suggestions(suggestions));
}

exports.handleError = (error, response, body, conv) => {
  // Handle errors here
  console.log(response);
  if (!response) {
    conv.close("An unknown error occured. Please contact our support.\nError: " + error);
  } else {
    if (response.statusCode == 400) {
      //Handle API errors that come with their own error messages
      //This basically just wraps existing messages in a more readable format for the Assistant
      if (error.toLowerCase().startsWith("unable")) {
        if (error.toLowerCase().includes("IATA")) {
          //Format "Unable to find the airports. Please use IATA airport codes only"
          conv.ask("I couldn't find that airport. Please only give me IATA codes.");
        } else {
          //Format "Unable to find <emission type> for <item type> in <region>"
          conv.ask("I was " + error + ". Please try again.");
        }
      } else if (error.toLowerCase().startsWith("please provide")) {
        //Format "Please provide valid sector and region values"
        conv.ask("Sorry, I'm missing some info from you. " + error + ".");
      } else if (error.toLowerCase().includes("cannot be less than zero")) {
        //Format "Distance cannot be less than zero"
        conv.ask("Sorry, I can't use a negative distance or mileage. Please try again.");
      } else {
        conv.close("An unknown error occured. Please report this to the developers.\nError: " + error);
      }
    } else if (response.statusCode == 403 || response.statusCode == 406) {
      // Forbidden, not acceptable
      conv.close("An unknown error occured. Please report this to the developers.\nError: " + error);
    } else if (response.statusCode == 404) {
      // Not found
      conv.ask("The data you requested isn't available. Please try again");
    } else if (response.statusCode == 429) {
      // Too many requests
      conv.close("I'm feeling a bit overwhelmed right now. Try asking me again later.");
    } else if (response.statusCode == 500) {
      // Internal server error
      conv.close("There's a problem with our server. Please try again in a bit.");
    } else if (response.statusCode == 503) {
      // Service unavailable
      conv.close("The server is currently offline for maintenance. Please try again later.");
    } else {
      conv.close("An unknown error occured. Please contact our support.\nError: " + error);
    }
  }
}
